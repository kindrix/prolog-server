:- use_module(library(http/thread_httpd)).

:- use_module(library(http/http_dispatch)).

:- use_module(library(http/html_write)).

% we need this module from the HTTP client library for http_read_data
:- use_module(library(http/http_client)).

server(Port) :-
        http_server(http_dispatch, [port(Port)]).

%new abstract base
:- multifile http:location/3.
:- dynamic   http:location/3.

%new abstract base 'files' mapped to '/f'
http:location(files, '/f', []).

%root path change
%http:location(prefix, '/f', []).


%handlers
:- http_handler(root(.), say_hi, []).
:- http_handler(root(show_image), show_image, []).
:- http_handler(files(bunny), say_bunny , []).
:- http_handler(root(show_form), web_form, []).
:- http_handler(root(landing), landing_pad, []).

%handler definition
say_hi(_Request) :-
    format('Content-type: text/html~n~n'),
    phrase(
    html(
    [head(title('Howdy')),
     body([h1('A Simple Web Page'),
          p('With some text')])
        ]),
    TokenizedHtml,
    []),
    print_html(TokenizedHtml).

    web_form(_Request) :-
    	reply_html_page(
    	    title('POST demo'),
    	    [
    	     form([action='/landing', method='POST'], [
    		p([], [
    		  label([for=name],'Name:'),
    		  input([name=name, type=textarea])
    		      ]),
    		p([], [
    		  label([for=email],'Email:'),
    		  input([name=email, type=textarea])
    		      ]),
    		p([], input([name=submit, type=submit, value='Submit'], []))
    	      ])]).

landing_pad(Request) :-
        member(method(post), Request), !,
        http_read_data(Request, Data, []),
        format('Content-type: text/html~n~n', []),
      	format('<p>', []),
              portray_clause(Data),
      	format('</p><p>========~n', []),
      	portray_clause(Request),
      	format('</p>').

say_bunny(_Request) :-
format('Content-type: text/plain~n~n'),
        format('Bunny!~n').


show_image(_Request) :- format('Content-type: text/html~n~n', []),
              format('<html>~n', []),
              format('<img src="http://www.swi-prolog.org/pldoc/package/httpserver.gif">',[]),
              format('</html>~n', []).
